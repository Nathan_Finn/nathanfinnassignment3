<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">
<concept id="concept_j32_lml_ybb">
 <title>Configuring data collection: DataPower SOA Appliance</title>
 <shortdesc>This section describes the support for monitoring of service flows through an IBM®
  WebSphere® DataPower® SOA appliance, where the ITCAM for SOA data collector acts as a proxy
  between the services clients and servers.</shortdesc>
 <prolog>
  <author>Nathan Finn</author>
  <copyright>
   <copyryear year="2017"/>
   <copyrholder>CIT</copyrholder>
  </copyright>
  <critdates>
   <created date="14/11/2017"/>
   <revised modified="16/11/2017"/>
   <revised modified="18/11/2017"/>
   <revised modified="19/11/2017"/>
   <revised modified="21/11/2017"/>
   <revised modified="23/11/2017"/>
   <revised modified="24/11/2017"/>
   <revised modified="25/11/2017"/>
   <revised modified="26/11/2017"/>
  </critdates>
  <metadata>
   <audience type="Public"/>
   <keywords conkeyref="keyword/kwd1"></keywords>
   <keywords conkeyref="keyword/kwd2"></keywords>
   <keywords conkeyref="keyword/kwd3"></keywords>
   <keywords conkeyref="index/kw2"></keywords>
   <keywords conkeyref="index/kw4"></keywords>
  </metadata>
 </prolog>
 <conbody>
  <p>The list of versions of the DataPower SOA appliance supported by ITCAM for SOA 7.2 is available
   from the Software product compatibility reports website. For information about accessing reports
   from this website, see <xref keyref="requiredSoftware">Required software</xref>.</p>
  <p>The DataPower data collector can be integrated with ITCAM for Transactions. If configured,
   transaction events are sent to a Transaction Collector which stores and aggregates transaction
   data from multiple data collectors. For more information about configuring the interface to ITCAM
   for Transactions, see <xref keyref="integratingWithITCAMForTransactions">Integrating with ITCAM for Transactions</xref>.</p>
  <p>IBM WebSphere DataPower SOA appliances are used for processing XML messages, and providing
   message transformation, services acceleration, and security functions. A DataPower appliance is
   typically used to improve the security and performance of services by offloading functions from
   the application server that is hosting the target service to a DataPower SOA appliance. Typical
   functions that are off-loaded include; authentication and authorization, XML schema validation,
   and services encryption and decryption.</p>
  <p>ITCAM for SOA provides a DataPower data collector that operates as a proxy and monitors
   services flows through a DataPower SOA appliance, providing similar services management and
   availability information that ITCAM for SOA currently provides for application server runtime
   environments. This information is displayed in the Tivoli® Enterprise Portal using the usual
   predefined or user-defined workspaces and views.</p>
  <p>DataPower supports two proxy types that can process SOA messages:</p>
  <dl>
   <dlentry>
    <dt>The Web Services Proxy</dt>
    <dd>You configure a Web Services Proxy by importing one or more WSDL files and then telling the
     appliance where to direct those messages. Thus, the Web Services Proxy receives only SOAP
     messages.</dd>
   </dlentry>
   <dlentry>
    <dt>The Multi-Protocol Gateway</dt>
    <dd>The Multi-Protocol Gateway is more versatile than the Web Services Proxy. You can use it to
     process nearly any type of message, including SOAP, non-SOAP XML, text, or binary. For XML
     messages (including SOAP), XSL transforms are used to manipulate the message. For non-XML
     messages, similar transform actions can be built using IBM WebSphere Transformation Extender
     (for more information about this product, see <xref keyref="iBMWebSphereTransformationExtender">IBM WebSphere Transformation Extender</xref>).</dd>
   </dlentry>
  </dl>
  <section id="section_jvr_d4l_ybb">
   <title>Upgrade your firmware</title>
   <p>Before using the DataPower data collector proxy, you must upgrade the firmware on DataPower
    SOA appliances that you want to monitor, to include the necessary monitoring and data
    transformation capabilities.</p>
   <note type="important" rev="1.0">
    <ul id="ul_ac3_x4l_ybb">
     <li>Upgrade your firmware to at least version 3.7.1 or later to monitor traffic through the Web
      Services Proxy.</li>
     <li>Upgrade your firmware to at least version 3.7.1 Fix Pack 4 or later to monitor traffic
      through a Multi-Protocol Gateway.</li>
    </ul>
   </note>
   <p>Consult your DataPower Appliance documentation for information about upgrading your firmware
    level.</p>
  </section>
 </conbody>
</concept>
