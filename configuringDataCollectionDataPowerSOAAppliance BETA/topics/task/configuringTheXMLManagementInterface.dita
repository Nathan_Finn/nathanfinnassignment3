<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE task PUBLIC "-//OASIS//DTD DITA Task//EN" "task.dtd">
<task id="task_bqh_blx_ybb">
    <title>Configuring the XML Management Interface</title>
    <prolog>
        <author>Nathan Finn</author>
        <copyright>
            <copyryear year="2017"/>
            <copyrholder>CIT</copyrholder>
        </copyright>
        <critdates>
            <created date="14/11/2017"/>
            <revised modified="16/11/2017"/>
            <revised modified="18/11/2017"/>
            <revised modified="19/11/2017"/>
            <revised modified="21/11/2017"/>
            <revised modified="23/11/2017"/>
            <revised modified="24/11/2017"/>
            <revised modified="25/11/2017"/>
            <revised modified="26/11/2017"/>
        </critdates>
        <metadata>
            <audience type="Public"/>
            <keywords conkeyref="keyword/kwd1"></keywords>
            <keywords conkeyref="keyword/kwd3"></keywords>
            <keywords conkeyref="keyword/kwd11"></keywords>
            <keywords conkeyref="index/kw2"></keywords>
            <keywords conkeyref="index/kw11"></keywords>
        </metadata>
    </prolog>
    <taskbody>
        <context audience="expert">
            <p>The XML Management Interface on the appliance must be enabled using the DataPower®
                administration console. To configure the XML Management interface, complete the
                following steps:</p>
        </context>
        <steps>
            <step>
                <cmd>Start the DataPower administration console in a web browser
                        (<codeph>https://hostname:9090/login.xml</codeph>).</cmd>
            </step>
            <step>
                <cmd>Complete the following steps to enable the XML Management interface.</cmd>
                <substeps id="substeps_qny_llx_ybb">
                    <substep>
                        <cmd>Log in to the administration console as the admin user for the default
                            domain.</cmd>
                    </substep>
                    <substep>
                        <cmd>Navigate to <menucascade>
                                <uicontrol>Objects</uicontrol>
                                <uicontrol>Management</uicontrol>
                                <uicontrol>XML Management Interface</uicontrol>
                            </menucascade>.</cmd>
                    </substep>
                    <substep>
                        <cmd>Make note of the port number that is displayed. You must specify this
                            port number later when you enable or disable data collection.</cmd>
                    </substep>
                    <substep>
                        <cmd>In the <uicontrol>Main</uicontrol> tab, find the
                                <uicontrol>WS-Management Endpoint</uicontrol> option and select the
                            on check box.</cmd>
                    </substep>
                    <substep>
                        <cmd>Click <uicontrol>Apply</uicontrol> to activate the changes and enable
                            the WS-Management Endpoint.</cmd>
                    </substep>
                </substeps>
            </step>
            <step>
                <cmd>Complete the following steps to configure the Web Services Agent for the
                    default domain:</cmd>
                <substeps id="substeps_zsx_3mx_ybb">
                    <substep id="a">
                        <cmd>Navigate to <menucascade>
                                <uicontrol>Services</uicontrol>
                                <uicontrol>Miscellaneous</uicontrol>
                                <uicontrol>Web Services Agent</uicontrol>
                            </menucascade>. For example:</cmd>
                        <info>
                            <fig id="fig_rfx_l4x_ybb">
                                <title>Configure Web Services Management Agent page</title>
                                <image keyref="configureWebServicesManagementAgentPage" />
                            </fig>
                        </info>
                    </substep>
                    <substep id="b">
                        <cmd>Set <uicontrol>Administrative State</uicontrol> to
                                <userinput>enabled</userinput>.</cmd>
                    </substep>
                    <substep id="c">
                        <cmd>Set the set <uicontrol>Buffering Mode</uicontrol> option to
                                <userinput>discard</userinput> or <userinput>buffer</userinput>. The
                            default setting is <userinput>discard</userinput>.</cmd>
                        <info>
                            <p>When <uicontrol>Buffering Mode</uicontrol> is set to
                                    <userinput>buffer</userinput>, the Web Services Agent buffers
                                transaction information for the current domain when no registered
                                ITCAM for SOA data collectors are running. Buffering reduces the
                                loss of transaction information, but consumes more memory.
                                Transaction records are buffered until the configured size limits
                                are reached. Buffering is a better choice when initially configuring
                                data collection, when processing high volumes of data, or when there
                                are multiple ITCAM for SOA subscribers.</p>
                            <p>When <uicontrol>Buffering Mode</uicontrol> is set to
                                    <userinput>discard</userinput>, transaction information from the
                                current domain is discarded when no registered ITCAM for SOA data
                                collectors are running. Complications can occur if a new ITCAM for
                                SOA subscriber replaces a former subscriber. High volumes of
                                transactions might cause the Complete Records Count to reach the
                                configured Maximum Record Size limit and transaction information to
                                be discarded. Setting Buffering Mode to
                                    <userinput>discard</userinput>is suited to an environment where
                                there is a single ITCAM for SOA subscriber, where the DataPower
                                appliance is handling a low volume of transaction data, and where
                                the ITCAM for SOA subscriber is collecting a low volume of metrics.
                                For information about troubleshooting scenarios where transaction
                                metrics are being discarded, see the <i>IBM® Tivoli® Composite
                                    Application Manager Troubleshooting Guide</i>.</p>
                        </info>
                    </substep>
                    <substep id="d">
                        <cmd>Adjust the values for <uicontrol>Maximum Record Size</uicontrol> and
                                <uicontrol>Maximum Memory Usage</uicontrol>, if necessary.</cmd>
                    </substep>
                    <substep id="e">
                        <cmd>If you want the data collector to record message content in addition to
                            summary metrics, change <uicontrol>Capture Mode</uicontrol> from
                                <userinput>faults</userinput> to
                            <userinput>all-messages</userinput>.</cmd>
                    </substep>
                </substeps>
            </step>
            <step>
                <cmd>Configure the Web Services Agent for <i>all other domains</i> that are
                    monitored by the DataPower data collector. For each domain, switch to the domain
                    and complete step <xref href="#task_bqh_blx_ybb/a">3.a</xref> to step <xref
                        href="#task_bqh_blx_ybb/e">3.e</xref>.</cmd>
            </step>
        </steps>
    </taskbody>
</task>
